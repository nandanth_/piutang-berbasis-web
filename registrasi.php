<?php
require 'functions.php';


if( isset($_POST["register"]) ) {

    if( registrasi($_POST) > 0 ) {
        echo "
            <script>
                alert('User Baru Berhasil ditambahkan!');
            </script>";
    } else {
        echo mysqli_error($conn);
    }
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrasi</title>

    <link rel="stylesheet" href="./style_login.css">
   
   
    </style>
</head>
<body>

<div class="global-container">
    <div class="card login-form">

    <div class="card-body">
         <h1 class="card-title text-center">R E G I S T R A S I</h1>
    </div>

    <div class="card-text">
    <form action="" method="post">

   
<div class="reg_box"></div>
    <div class="mb-3">
            <label for="username">Username </label>
            <input type="text" name="username" id="username" class="form-control" required>
        </div>


            <div class="mb-3">
                    <label for="password">Password :</label>
                    <input type="password" name="password" id="password"  class="form-control" required>
             </div>

            <div class="mb-3">
                    <label for="password2">Konfirmasi Password :</label>
                    <input type="password" name="password2" id="password2"  class="form-control" required>
            </div>

            

            <div class="d-grid gap-2">
            <button type="submit" name="register" class="btn btn-primary">Register!</button>
        </div>
    </form>


    
 </div>
 </div>

 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</body>
</html>