/*
SQLyog Trial v13.1.9 (64 bit)
MySQL - 10.4.27-MariaDB : Database - piutang_web
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tabel_customer` */

DROP TABLE IF EXISTS `tabel_customer`;

CREATE TABLE `tabel_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(100) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `no_telp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

/*Data for the table `tabel_customer` */

insert  into `tabel_customer`(`id`,`nama_customer`,`alamat`,`no_telp`) values 
(8,'Dominco Inc','pinus st. 45','+61 56999'),
(10,'viscout inc','stain st. 55','+66 8999 665');

/*Table structure for table `tabel_pembayaran` */

DROP TABLE IF EXISTS `tabel_pembayaran`;

CREATE TABLE `tabel_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `jumlah_bayar` int(100) DEFAULT NULL,
  `metode_bayar` varchar(1000) DEFAULT NULL,
  `ket_bayar` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pelanggan` (`id_pelanggan`),
  CONSTRAINT `tabel_pembayaran_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `tabel_piutang` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

/*Data for the table `tabel_pembayaran` */

insert  into `tabel_pembayaran`(`id`,`id_pelanggan`,`tanggal_bayar`,`jumlah_bayar`,`metode_bayar`,`ket_bayar`) values 
(14,5,'2023-06-18',50000,'cash','gdgdgd'),
(15,5,'2023-06-19',50000,'jasa','fdfsfsf'),
(18,5,'0000-00-00',10000,'cash','ssdsds'),
(19,5,'0000-00-00',20000,'transfer','dsdsdsdsddd'),
(20,5,'2023-06-23',10000,'giro','bshdhsdnbbsjdhusdsjdshdsd'),
(23,5,'2023-06-22',20000,'jasa','sdsdsdsdsdsds'),
(24,5,'2023-06-24',20000,'transfer','njusaiusauisioas'),
(25,5,'2023-06-26',10000,'cash','kinjkaosdsudmksdjudmkncjxzdjosiodfsjkcjxciuoxjcj  bsjaidijsdjksjiod osdosdojsjokdsd'),
(30,5,'2023-06-30',1000,'cash',''),
(32,8,'0000-00-00',100000,'cash','');

/*Table structure for table `tabel_piutang` */

DROP TABLE IF EXISTS `tabel_piutang`;

CREATE TABLE `tabel_piutang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `nomor_invoice` varchar(200) DEFAULT NULL,
  `tanggal_piutang` date DEFAULT NULL,
  `tanggal_tempo` date DEFAULT NULL,
  `umur_piutang` int(200) DEFAULT NULL,
  `nominal` int(200) DEFAULT NULL,
  `sisa_piutang` int(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tabel_piutang_ibfk_1` (`id_customer`),
  CONSTRAINT `tabel_piutang_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `tabel_customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

/*Data for the table `tabel_piutang` */

insert  into `tabel_piutang`(`id`,`id_customer`,`nomor_invoice`,`tanggal_piutang`,`tanggal_tempo`,`umur_piutang`,`nominal`,`sisa_piutang`) values 
(5,8,'abc4888','2023-06-18','2023-06-18',NULL,100000,9000),
(7,8,'ja4889954','2023-06-23','2023-07-08',NULL,800000,0),
(8,10,'abc4888','2023-07-04','2023-07-04',NULL,100000,0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`) values 
(3,'nath','$2y$10$DuPNDXjgd1iICLsbmrLG.OmRqoy4vhe3vxP3oGxqQyju8RDQeZsUq');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
